# PELATIHAN CRUD DASAR #

Pada project ini, merupakan contoh crud untuk program sederhana. DBMS yang digunakan MySql

### Instalasi ###

* Buat database dengan nama yang anda sukai, dalam hal ini saya pakai nama "pelatihan"
* Import database ke dalam mysql, salahsatu tutorialnya bisa dilihat di http://www.inmotionhosting.com/support/website/phpmyadmin/import-database-using-phpmyadmin
* Konfigurasi koneksi database terdapat pada file ./function.php
* Defaultnya untuk masuk sistem bisa menggunakan username: "admin" dan  password: "1234"